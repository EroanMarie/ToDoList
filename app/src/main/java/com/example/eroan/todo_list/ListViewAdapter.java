package com.example.eroan.todo_list;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Eroan on 28/05/2018.
 */

public class ListViewAdapter extends ArrayAdapter {
    private final Activity context;
    private final JSONArray[] itemDescr;
    String[] itemName;
    public ListViewAdapter(Activity context, JSONArray[] itemDescr) {
        super(context, R.layout.todos_layout, itemDescr);
        // TODO Auto-generated constructor stub
        this.context=context;
        this.itemDescr=itemDescr;
        for(int i = 0; i<itemDescr.length; i++){
            System.out.println(i + " : " + itemDescr[i]);
        }
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View todoView=inflater.inflate(R.layout.todos_layout, null,true);
        CheckBox checkBox = (CheckBox) todoView.findViewById(R.id.checkBox);
        try {
            if(itemDescr[position].get(3).equals(true)){
                checkBox.setChecked(true);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            checkBox.setText(String.valueOf(itemDescr[position].get(0)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            switch (String.valueOf(itemDescr[position].get(2))){
                case "Faible" :
                    checkBox.setTextColor(Color.GREEN);
                    break;
                case "Moyenne" :
                    checkBox.setTextColor(Color.YELLOW);
                    break;
                case "Haute" :
                    checkBox.setTextColor(Color.RED);
                    break;
                default :
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return todoView;
    }
}