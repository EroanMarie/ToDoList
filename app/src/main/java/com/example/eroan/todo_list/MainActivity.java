package com.example.eroan.todo_list;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    JSONArray datas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String testErr = read("todos");
        System.out.println(testErr);
        if(testErr == "err"){
            write("", "todos");
        }else{
            try {
                datas = new JSONArray(read("todos"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(getIntent().getExtras()!=null) {
            if(datas == null){
                datas = new JSONArray();
            }
            datas.put(getIntent().getExtras().getString("Values"));
            System.out.println(String.valueOf(datas));
            write(String.valueOf(datas), "todos");
            System.out.println("read : " + read("todos"));
        }

        if(!testErr.equals("err") && datas != null) {
            final ListView mCompteListView = (ListView) findViewById(R.id.listView);

            final JSONArray[] adapterJson = new JSONArray[datas.length()];
            for (int i = 0; i < datas.length(); i++) {
                try {
                    adapterJson[i] = new JSONArray(datas.get(i).toString());
                    System.out.println(adapterJson[i]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            final ListViewAdapter adapter = new ListViewAdapter(this, adapterJson);
            mCompteListView.setAdapter(adapter);

            mCompteListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub;
                    CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox);
                    cb.setChecked(!cb.isChecked());
                    System.out.println("click on " + position);
                    try {
                        adapterJson[position].put(3, !adapterJson[position].getBoolean(3));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    System.out.println(String.valueOf(adapterJson));
                    String a = "";
                    for (int i = 0; i < adapterJson.length; i++) {
                        if(i < adapterJson.length-1) {
                            a += String.valueOf(adapterJson[i]) + ",";
                        }else{
                            a += String.valueOf(adapterJson[i]);
                        }
                    }
                    write("[" + a + "]", "todos");
                    System.out.println("last read : " + read("todos"));
                }
            });
            mCompteListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                    try {
                        alertDialog.setTitle(String.valueOf(adapterJson[position].get(0)));
                        alertDialog.setMessage(String.valueOf(adapterJson[position].get(1)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                    return false;
                }
            });
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        getIntent().removeExtra("Values");
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addTODO:
                addTODO();
                break;
            case R.id.infos:
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Informations");
                alertDialog.setMessage("Maintenir le clic sur un objet permet d'afficher sa description" +
                        "\nPriorités :\nVert : Faible\nJaune : Moyenne\nRouge : Haute");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void addTODO(){
        Intent intent = new Intent(MainActivity.this, AddActivity.class);
        startActivity(intent);
    }

    public void write(String text, String fichier) {
        try {
            FileOutputStream fileOutputStream = openFileOutput(fichier+".json",MODE_PRIVATE);
            fileOutputStream.write(text.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String read(String fichier) {
        try {
            FileInputStream fileInputStream= openFileInput(fichier+".json");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();
            String lines;
            while ((lines=bufferedReader.readLine())!=null) {
                stringBuffer.append(lines+"\n");
            }
            //System.out.println(String.valueOf(stringBuffer));
            return String.valueOf(stringBuffer);

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("erreur");
        return "err";
    }
}
