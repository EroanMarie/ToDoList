package com.example.eroan.todo_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Eroan on 18/05/2018.
 */

public class AddActivity extends AppCompatActivity implements View.OnClickListener{

    Spinner priorities;
    EditText title, description;
    Button create;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        priorities = (Spinner) findViewById(R.id.priority);
        title = (EditText) findViewById(R.id.title);
        description = (EditText) findViewById(R.id.description);
        create = (Button) findViewById(R.id.create);
        create.setOnClickListener(this);

        //Utiliser le fichier priorities.xml pour déclarer tous les objets de la liste déroulante
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.priorities_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        priorities.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.create:
                create();
                break;
            default:
                break;
        }
    }

    public void create(){
        if (!String.valueOf(title.getText()).equals("")){
            JSONArray values = new JSONArray();
            try {
                values.put(0, String.valueOf(title.getText()));
                values.put(1, String.valueOf(description.getText()));
                values.put(2, String.valueOf(priorities.getSelectedItem()));
                values.put(3, false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // values[0] => titre, values[1] => description, values[2] => priorité, values[3] => checked
            Intent intent = new Intent(AddActivity.this,MainActivity.class);
            intent.putExtra("Values",String.valueOf(values));
            startActivity(intent);
        }
    }
}
